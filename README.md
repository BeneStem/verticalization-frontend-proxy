# frontend-proxy

- Install Docker
- `docker build -f Dockerfile -t frontend-proxy:latest .`
- `docker run --rm -p 8081:80 frontend-proxy`

Open: [Product Detail Page](http://localhost:8081/discover/products)
